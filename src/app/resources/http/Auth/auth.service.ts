import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/User/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}
  url = 'http://localhost:3000/users';

  login(username: string, password: string) {
    return this.http.get<User[]>(this.url, {
      params: {
        username_like: username,
        password_like: password,
      },
    });
  }

  logout() {
    return localStorage.removeItem('user');
  }

  register(user: User) {
    return this.http.post<User>(this.url, user);
  }

  setUser(user: User) {
    return localStorage.setItem('user', user.email);
  }

  isLogged() {
    return localStorage.getItem('user') !== null;
  }
}
