import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../../models/Product/product';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchModel } from '../../models/Search/search-model';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private http: HttpClient) {}
  private headers = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private url = 'http://localhost:3000/products';

  private getPutDeleteUrl(id: number) {
    return `${this.url}/${id}`;
  }



  public getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url);
  }

  public getProductsByType(model: SearchModel): Observable<Product[]> {
    const params: any = {
      type: model.type,
      _page: model._page,
      _limit: model._limit,
    };

    if (model.query != null) {
      params.title_like = model.query;
    }

    if (model.min != null) {
      params.price_gte = model.min;
    }

    if (model.max != null) {
      params.price_lte = model.max;
    }

    if (model._page != null) {
      params._page = model._page;
    }

    if (model._limit != null) {
      params._limit = model._limit;
    }

    return this.http.get<Product[]>(this.url, {
      params,
    });
  }

  public createProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.url, product, this.headers);
  }

  public findProduct(id: number): Observable<Product> {
    return this.http.get<Product>(`${this.url}/${id}`);
  }

  public updateProduct(product: Product): Observable<Product> {
    const productUrl = this.getPutDeleteUrl(product.id);
    return this.http.put<Product>(productUrl, product, this.headers);
  }

  public deleteProduct(id: number): Observable<{}> {
    const productUrl = this.getPutDeleteUrl(id);
    return this.http.delete(productUrl, this.headers);
  }

  public getCount(type: string) {
    return this.http
      .get<Product[]>(this.url, {
        params: { type },
      })
      .pipe(map(value => value.length));
  }
}
