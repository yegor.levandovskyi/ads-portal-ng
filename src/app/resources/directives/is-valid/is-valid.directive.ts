import { Directive, HostBinding } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: 'input',
})
export class IsValidDirective {
  constructor(private ngControl: NgControl) {}

  @HostBinding('class.is-success')
  get isInvalid() {
    return this.ngControl.control.valid && this.ngControl.control.touched;
  }
}
