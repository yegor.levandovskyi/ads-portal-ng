import { Directive, HostBinding } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: 'input',
})
export class IsInvalidDirective {
  constructor(private ngControl: NgControl) {}

  @HostBinding('class.is-danger')
  get isInvalid() {
    return this.ngControl.control.invalid && this.ngControl.control.touched;
  }
}
