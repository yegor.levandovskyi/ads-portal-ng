export interface SearchModel {
  query?: string,
  min?: number,
  max?: number,
  _page: number,
  _limit: number,
  type?: string
}
