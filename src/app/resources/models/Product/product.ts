export interface Product {
  id?: number,
  type: string;
  title: string;
  price: number;
  description: string;
  src: string;
  srcImgName: string;
  created: Date;
  updated?: Date;
}
