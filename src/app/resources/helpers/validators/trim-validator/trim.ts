import { ValidatorFn, AbstractControl } from '@angular/forms';

export const TrimValidator: ValidatorFn = (control: AbstractControl) => {
  if (control.value.startsWith(' ')) {
    return {
      'trimError': { value: 'control has leading whitespace' }
    };
  }
  if (control.value.endsWith(' ')) {
    return {
      'trimError': { value: 'control has trailing whitespace' }
    };
  }

  return null;
};

