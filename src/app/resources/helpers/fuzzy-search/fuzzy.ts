import { Product } from '../../models/Product/product';

export const fuzzy = (input: string = '', data: Product[] = []) => {
  const regex = RegExp(input.split('').join('.*'));
  const suggestions = [];

  data.forEach(e => {
    const match = regex.exec(e.title.toLowerCase());
    if(match) suggestions.push({e, length: match[0].length, start: match.index});
  })

  suggestions.sort((a, b) => {
    if(a.length < b.length) return -1;
    if(a.length > b.length) return 1;
    if(a.start < b.start) return -1;
    if(a.start > b.start) return 1;
    return 0;
  })

  return suggestions.map(elem => elem.e);
}