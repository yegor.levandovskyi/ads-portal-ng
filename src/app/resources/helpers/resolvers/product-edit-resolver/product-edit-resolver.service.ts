import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';
import { Product } from '../../../models/Product/product';
import { ProductsService } from '../../../http/Products/products.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProductEditResolverService implements Resolve<Product> {
  constructor(private ps: ProductsService, private router: Router) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> | Observable<never> {
    let id = Number(route.paramMap.get('id'));

    return this.ps.findProduct(id).pipe(
      mergeMap((product: Product) => {
        if (product) return of(product);
      }),
      catchError((err: HttpErrorResponse) => {
        console.log(err);
        this.router.navigate(['/sections/men']);
        return EMPTY;
      })
    );
  }
}
