import { TestBed } from '@angular/core/testing';

import { ProductEditResolverService } from './product-edit-resolver.service';

describe('ProductEditResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductEditResolverService = TestBed.get(ProductEditResolverService);
    expect(service).toBeTruthy();
  });
});
