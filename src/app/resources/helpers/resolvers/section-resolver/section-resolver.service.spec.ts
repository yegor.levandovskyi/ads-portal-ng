import { TestBed } from '@angular/core/testing';

import { SectionResolverService } from './section-resolver.service';

describe('SectionResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SectionResolverService = TestBed.get(SectionResolverService);
    expect(service).toBeTruthy();
  });
});
