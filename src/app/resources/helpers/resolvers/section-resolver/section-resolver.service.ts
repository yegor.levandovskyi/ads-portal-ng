import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';

import { ProductsService } from '../../../http/Products/products.service';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class SectionResolverService implements Resolve<{ count: number }> {
  constructor(private ps: ProductsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let type = route.paramMap.get('type');
    return this.ps.getCount(type).pipe(map(count => ({ count })));
  }
}
