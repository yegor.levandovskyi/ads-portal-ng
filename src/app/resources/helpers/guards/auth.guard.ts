import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../../http/Auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  // Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const routerState = '_routerState';
    const url = 'url';
    const redirectUrl = route[routerState][url];

    if (this.authService.isLogged()) {
      return true;
    }

    return this.router.createUrlTree(['/auth/login'], {
      queryParams: {
        redirectUrl,
      },
    });
  }
}
