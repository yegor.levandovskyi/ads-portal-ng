import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavComponent } from './components/core/nav/nav.component';
import { FooterComponent } from './components/core/footer/footer.component';
import { RecentAdComponent } from './components/core/footer/recent-ad/recent-ad.component';

import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './modules/shared/shared.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ProductsService } from './resources/http/Products/products.service';
import { AuthGuard } from './resources/helpers/guards/auth.guard';

const appRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'sections',
        loadChildren: () =>
          import('./modules/section/section.module').then(
            mod => mod.SectionModule
          ),
      },
      {
        path: 'controls',
        loadChildren: () =>
          import('./modules/add-edit/add-edit.module').then(
            mod => mod.AddEditModule
          ),
      },
    ],
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./modules/auth/auth.module').then(mod => mod.AuthModule),
  },
  { path: '', redirectTo: 'sections', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    RecentAdComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],
  exports: [SharedModule],
  providers: [ProductsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
