import { Component, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { AuthService } from 'src/app/resources/http/Auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: [
    './nav.component.scss',
    '../../../../../node_modules/bulma/css/bulma.css',
  ],
})
export class NavComponent {
  constructor(
    private renderer: Renderer2,
    private authService: AuthService,
    private router: Router
  ) {}
  @ViewChild('mobileMenu', { static: true }) menu: ElementRef<HTMLDivElement>;
  open = false;

  toggleNavigation() {
    this.open = !this.open;
    this.openMenu();
  }

  openMenu() {
    if (this.menu.nativeElement.style.maxHeight) {
      this.renderer.removeStyle(this.menu.nativeElement, 'max-height');
    } else {
      this.renderer.setStyle(
        this.menu.nativeElement,
        'max-height',
        this.menu.nativeElement.scrollHeight + 'px'
      );
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }
}
