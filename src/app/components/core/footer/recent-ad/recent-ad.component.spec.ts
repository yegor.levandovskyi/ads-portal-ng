import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentAdComponent } from './recent-ad.component';

describe('RecentAdComponent', () => {
  let component: RecentAdComponent;
  let fixture: ComponentFixture<RecentAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentAdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
