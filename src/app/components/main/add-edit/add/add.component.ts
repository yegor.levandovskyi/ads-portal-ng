import { Component } from '@angular/core';
import { ProductsService } from 'src/app/resources/http/Products/products.service';
import { Product } from 'src/app/resources/models/Product/product';
import { Validators } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { TrimValidator } from 'src/app/resources/helpers/validators/trim-validator/trim';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: [
    '../shared/add-edit.style.scss',
    '../../../../../../node_modules/bulma/css/bulma.css',
  ],
})
export class AddComponent {
  constructor(private http: ProductsService, private router: Router) {}

  product: Product;

  imgSrc = [
    {
      name: 'Man t-shirt',
      src: '../../../../assets/img/men.png',
    },
    {
      name: 'Woman t-shirt',
      src: `../../../../assets/img/women.jpg`,
    },
  ];

  types = ['men', 'women'];

  productForm = new FormGroup({
    type: new FormControl('', Validators.required),
    title: new FormControl('', [Validators.required, TrimValidator]),
    price: new FormControl('', Validators.required),
    description: new FormControl(''),
    imgSelect: new FormControl('', Validators.required),
  });

  onSubmit(f: FormGroup) {
    const imgName = this.imgSrc.find(img => img.src === f.value.imgSelect).name;
    this.product = {
      type: f.value.type,
      title: f.value.title,
      price: f.value.price,
      description: f.value.description,
      src: f.value.imgSelect,
      srcImgName: imgName,
      created: new Date(),
    };

    this.http.createProduct(this.product).subscribe(product => {
      alert(`'${product.title}' successfully added`);
      this.router.navigate(['/sections/men']);
    });
  }
}
