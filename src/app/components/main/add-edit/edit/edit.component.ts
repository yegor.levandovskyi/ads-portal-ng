import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/resources/http/Products/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/resources/models/Product/product';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TrimValidator } from 'src/app/resources/helpers/validators/trim-validator/trim';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: [
    '../shared/add-edit.style.scss',
    '../../../../../../node_modules/bulma/css/bulma.css',
  ],
})
export class EditComponent implements OnInit {
  constructor(
    private ps: ProductsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  product: Product;

  editProductForm = new FormGroup({
    type: new FormControl('', Validators.required),
    title: new FormControl('', [Validators.required, TrimValidator]),
    price: new FormControl('', Validators.required),
    description: new FormControl(''),
    src: new FormControl('', Validators.required),
  });

  imgSrc = [
    {
      name: 'Man t-shirt',
      src: '../../../../assets/img/men.png',
    },
    {
      name: 'Woman t-shirt',
      src: `../../../../assets/img/women.jpg`,
    },
  ];

  types = ['men', 'women'];

  saveProduct() {
    const imgName = this.imgSrc.find(img => img.src === this.product.src).name;
    const updatedProduct: Product = {
      ...this.editProductForm.value,
      id: this.product.id,
      srcImgName: imgName,
      created: this.product.created,
      updated: new Date(),
    };

    this.ps.updateProduct(updatedProduct).subscribe(product => {
      alert(`${product.title} succesfully updated!`);
      this.router.navigate(['/sections/men']);
    });
  }

  deleteProduct() {
    if (!confirm(`Are you sure you want to delete ${this.product.title}?`)) {
      return;
    }
    this.ps.deleteProduct(this.product.id).subscribe(data => {
      alert(`${this.product.title} successfully deleted!`);
      this.router.navigate(['/sections/men']);
    });
  }

  resolveProduct() {
    this.route.data.subscribe((data: { product: Product }) => {
      this.product = data.product;
      this.renderForm();
    });
  }

  renderForm() {
    this.editProductForm.patchValue(this.product);
  }

  ngOnInit() {
    this.resolveProduct();
    console.log(this.editProductForm.controls.title.errors);
  }
}
