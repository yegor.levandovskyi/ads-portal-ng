import { Component, OnInit, HostBinding, Directive } from '@angular/core';
import { AuthService } from 'src/app/resources/http/Auth/auth.service';
import { FormGroup, FormControl, Validators, NgControl } from '@angular/forms';
import { TrimValidator } from 'src/app/resources/helpers/validators/trim-validator/trim';
import { User } from 'src/app/resources/models/User/user';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    '../shared/style.scss',
    '../../../../../../node_modules/bulma/css/bulma.css',
  ],
})
export class RegisterComponent {
  constructor(private authService: AuthService, private router: Router) {}

  registerForm = new FormGroup({
    firstname: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      TrimValidator,
    ]),
    lastname: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      TrimValidator,
    ]),
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      TrimValidator,
    ]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      TrimValidator,
    ]),
  });

  user: User;

  registerUser() {
    if (this.registerForm.valid) {
      this.user = {
        ...this.registerForm.value,
      };
      this.authService.register(this.user).subscribe(
        res => {
          alert(`${this.user.username} is registered`);
          this.authService.setUser(this.user);
          this.router.navigate(['/sections/men']);
        },
        err => {
          alert('error registering user!');
          console.error(err);
        }
      );
    } else {
      console.log('invalid');
    }
  }
}
