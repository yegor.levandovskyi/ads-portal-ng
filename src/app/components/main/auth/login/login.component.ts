import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/resources/http/Auth/auth.service';
import { User } from 'src/app/resources/models/User/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    '../shared/style.scss',
    '../../../../../../node_modules/bulma/css/bulma.css',
  ],
})
export class LoginComponent {
  constructor(private auth: AuthService, private router: Router) {}

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  getLoginResponse(f: FormGroup) {
    this.auth
      .login(f.value.username, f.value.password)
      .subscribe((res: User[]) => {
        if (res.length > 0) {
          this.auth.setUser(res[0]);
          this.router.navigate(['/sections/men']);
        } else {
          console.log('no user');
        }
      });
  }
}
