import {
  Component,
  ViewChild,
  ElementRef,
  Renderer2,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

import { debounceTime, distinctUntilChanged, skip } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { SearchModel } from 'src/app/resources/models/Search/search-model';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent {
  @ViewChild('filters', { static: false }) filters: ElementRef<HTMLDivElement>;
  @Output() searchChange: EventEmitter<SearchModel> = new EventEmitter();

  @Input() set search(model: SearchModel) {
    this.searchModel.next(model);
  }

  open = false;

  readonly searchModel = new BehaviorSubject<SearchModel>({
    query: '',
    _page: 1,
    _limit: 3,
  });

  constructor(private renderer: Renderer2) {
    this.searchModel
      .pipe(
        skip(1),
        debounceTime(400),
        distinctUntilChanged((a, b) => {
          if (a.query !== b.query) {
            return false;
          }

          if (a.min !== b.min) {
            return false;
          }

          if (a.max !== b.max) {
            return false;
          }

          return true;
        })
      )
      .subscribe(value => {
        this.searchChange.emit(value);
      });
  }

  onSearchInputChange(query: string) {
    const snapshot = this.searchModel.getValue();

    this.searchModel.next({
      ...snapshot,
      query,
    });
  }

  onMinInputChange(min: string) {
    const snapshot = this.searchModel.getValue();
    const minNum = Number(min);

    this.searchModel.next({
      ...snapshot,
      min: minNum === 0 ? null : minNum,
    });
  }

  onMaxInputChange(max: string) {
    const snapshot = this.searchModel.getValue();
    const maxNum = Number(max);

    this.searchModel.next({
      ...snapshot,
      max: maxNum === 0 ? null : maxNum,
    });
  }

  toggleFilters() {
    this.open = !this.open;
    this.openFilters();
  }

  openFilters() {
    if (this.filters.nativeElement.style.maxHeight) {
      this.renderer.removeStyle(this.filters.nativeElement, 'max-height');
    } else {
      this.renderer.setStyle(
        this.filters.nativeElement,
        'max-height',
        this.filters.nativeElement.scrollHeight + 'px'
      );
    }
  }
}
