import { Component, Output, SimpleChanges, OnChanges, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { SearchModel } from 'src/app/resources/models/Search/search-model';

@Component({
  selector: 'app-limit',
  templateUrl: './limit.component.html',
  styleUrls: ['./limit.component.scss']
})
export class LimitComponent {
  @Output() limitChange: EventEmitter<number> = new EventEmitter();
  @Input() searchModel: SearchModel;

  itemsPerPage: number[] = [2, 3, 4];
  currentLimit: number;

  emitLimit() {
    this.limitChange.emit(this.currentLimit);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.searchModel) {
      this.currentLimit = this.searchModel._limit;
    }
  }
  
}
