import { Component, OnInit } from '@angular/core';
import {
  ActivatedRoute,
  Router,
} from '@angular/router';

import {
  Subscription,
  Observable,
  BehaviorSubject,
} from 'rxjs';
import { SearchModel } from 'src/app/resources/models/Search/search-model';
import { map, switchMap } from 'rxjs/operators';
import { ProductsService } from 'src/app/resources/http/Products/products.service';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss'],
})
export class SectionComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductsService,
  ) {}

  products$: Observable<any[]>;
  totalCount$: Observable<number>;
  searchSubscription: Subscription;

  searchSubject = new BehaviorSubject<SearchModel>({
    type: this.activatedRoute.snapshot.params.type,
    _page: 1,
    _limit: 3,
  });

  onPageChange(_page: number) {
    const snapshot = this.searchSubject.getValue();

    this.searchSubject.next({
      ...snapshot,
      _page,
    });
  }

  onLimitChange(_limit: number) {
    const snapshot = this.searchSubject.getValue();
    this.searchSubject.next({
      ...snapshot,
      _limit,
    });
  }

  onSearchChange(model: SearchModel) {
    const snapshot = this.searchSubject.getValue();

    this.searchSubject.next({
      ...snapshot,
      ...model
    });
  }

  ngOnInit() {
    const searchStream$ = this.searchSubject.pipe(
      switchMap(model => this.productService.getProductsByType(model))
    );

    this.activatedRoute.params
      .pipe(
        map(params => {
          return {
            type: params.type,
            _limit: 3,
            _page: 1,
          };
        })
      )
      .subscribe(this.searchSubject);

    this.totalCount$ = this.activatedRoute.data.pipe(
      map(value => value.list.count)
    );

    this.products$ = searchStream$;
  }
}
