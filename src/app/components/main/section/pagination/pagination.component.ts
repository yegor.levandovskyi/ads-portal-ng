import {
  Component,
  Input,
  SimpleChanges,
  Output,
  OnChanges,
} from '@angular/core';
import { SearchModel } from 'src/app/resources/models/Search/search-model';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnChanges {
  constructor() {}

  @Input() totalItems: number;
  @Input() searchModel: SearchModel;
  @Output() pageChange: EventEmitter<number> = new EventEmitter();

  pages: number[] = [];
  currentPage: number;
  currentLimit: number;

  updatePages() {
    const totalPages = Math.ceil(this.totalItems / this.searchModel._limit);
    this.pages = Array(totalPages)
      .fill(totalPages)
      .map((_, i) => i + 1);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.searchModel) {
      this.currentPage = this.searchModel._page;
      this.currentLimit = this.searchModel._limit;
      this.updatePages();
    }
  }

  emitPage(page: number) {
    this.pageChange.emit(page);
  }
}
