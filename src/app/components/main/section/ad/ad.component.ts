import { Component, Input } from '@angular/core';
import { Product } from 'src/app/resources/models/Product/product';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss'],
})
export class AdComponent {
  @Input() product: Product;

  dateDiffInDays(a: Date, b: Date) {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;

    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }

  checkIfNew() {
    const now = new Date();
    const createdDate = new Date(`${this.product.created}`);
    const dayDifference = this.dateDiffInDays(createdDate, now);

    return dayDifference <= 3;
  }

  checkIfEmpty() {
    return this.product.description === '';
  }
}
