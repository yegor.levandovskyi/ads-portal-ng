import { Component, ViewChild, ElementRef } from '@angular/core';
import { ProductsService } from './resources/http/Products/products.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Ads Portal';


  constructor() {

  }

  ngOnInit(): void {

  }

}
