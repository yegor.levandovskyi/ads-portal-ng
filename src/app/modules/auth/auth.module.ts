import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from 'src/app/components/main/auth/login/login.component';
import { RegisterComponent } from 'src/app/components/main/auth/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IsInvalidDirective } from 'src/app/resources/directives/is-invalid/is-invalid.directive';
import { IsValidDirective } from 'src/app/resources/directives/is-valid/is-valid.directive';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, IsInvalidDirective, IsValidDirective],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AuthModule { }
