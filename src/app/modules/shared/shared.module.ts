import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdComponent } from 'src/app/components/main/section/ad/ad.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';


@NgModule({
  declarations: [
    AdComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    AdComponent,
    CommonModule,
    RouterModule
  ]
})
export class SharedModule { }
