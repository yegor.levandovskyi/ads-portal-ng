import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditComponent } from 'src/app/components/main/add-edit/edit/edit.component';
import { AddComponent } from 'src/app/components/main/add-edit/add/add.component';
import { ProductEditResolverService } from 'src/app/resources/helpers/resolvers/product-edit-resolver/product-edit-resolver.service';

const routes: Routes = [
  { path: 'add', component: AddComponent },
  {
    path: 'edit/:id',
    component: EditComponent,
    resolve: {
      product: ProductEditResolverService,
    },
  },
  {path: '', redirectTo: 'add', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddEditRoutingModule { }
