import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddEditRoutingModule } from './add-edit-routing.module';

import { EditComponent } from 'src/app/components/main/add-edit/edit/edit.component';
import { AddComponent } from 'src/app/components/main/add-edit/add/add.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditComponent, AddComponent],
  imports: [
    CommonModule,
    AddEditRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class AddEditModule { }
