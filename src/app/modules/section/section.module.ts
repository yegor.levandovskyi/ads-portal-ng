import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SectionRoutingModule } from './section-routing.module';
import { SectionComponent } from 'src/app/components/main/section/section.component';
import { FiltersComponent } from 'src/app/components/main/filters/filters.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { PaginationComponent } from 'src/app/components/main/section/pagination/pagination.component';
import { LimitComponent } from 'src/app/components/main/section/limit/limit.component';
import { AuthGuard } from 'src/app/resources/helpers/guards/auth.guard';

@NgModule({
  declarations: [SectionComponent,FiltersComponent, PaginationComponent, LimitComponent],
  imports: [
    CommonModule,
    SectionRoutingModule,
    SharedModule,
    FormsModule,
  ],
  providers: [AuthGuard]
})
export class SectionModule { }
