import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SectionComponent } from 'src/app/components/main/section/section.component';
import { SectionResolverService } from 'src/app/resources/helpers/resolvers/section-resolver/section-resolver.service';

const routes: Routes = [
  {
    path: ':type',
    component: SectionComponent,
    resolve: {
      list: SectionResolverService,
    },
  },
  { path: '', redirectTo: 'men', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SectionRoutingModule {}
